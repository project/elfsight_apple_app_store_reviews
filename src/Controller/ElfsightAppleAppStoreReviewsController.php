<?php

namespace Drupal\elfsight_apple_app_store_reviews\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightAppleAppStoreReviewsController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/appleappstore-reviews/?utm_source=portals&utm_medium=drupal&utm_campaign=apple-app-store-reviews&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
